This native Android application provides information on Big Nerd Ranch's Blog Posts as per the coding challenge set forth by The Big Nerd Ranch.

This app implements a Model-View-ViewModel (MVVM) architecture using Retrofit with OkHttp clients for accessing RESTful Web APIs, GSON for data serialization, Timber for logging and Android Lifecycle and Architectural Components to achieve clean MVVM design.

The focus of this lightweight app is to demonstrate the use and understanding SOLID programming principles and the use of modern design and architecture.  The app is intentionally lightweight, and as such does not place great emphasis on UI design, test-driven development, or in-depth error handling that a normal enterprise application would implement.
The app does follow tiered and fragment architecture principles, separation of concerns, interface segregation, and includes unit testing.

Errors I noticed from the original app:
	* Some of the lifecycle components contained unnecessary imports which I removed
	* Network requests were being performed on the Main thread, which blocks the UI.  I fixed this with the introduction of Square's Retrofit and Kotlin coroutines
	* The app was using an outdated MVC architecture, which I converted to a modern MVVM style architecture
	* The app was not taking advantage of a tiered architecture, which I also solved using MVVM

Overall Architecture:
	* I redesigned the app to follow a Model, View, View Model (MVVM) architecture
	* I introduced a more fragmented architecture. Fragments are now used only for the UI, view models for business logic, a lightweight repository for data retrieval
	* I introduced Kotlin view models and and coroutines to enable reactivity in the app architecture
	* I updated all of the libraries to their most recent versions, and I incorporated the use of several additional libraries
	* I added the popular Picasso image loading library to efficiently load the author images from the network
	* I added Retrofit for handling data coming over the network
	* I added multiple additional to aid in unit testing of Kotlin coroutines
	* I moved the instantiation of the 'Service Generator', aka the retrofit client which handles network requests, within its own singleton class
	* I added a Splash screen and created a custom app launcher icon using Big Nerd Ranch's Aaron Hillegass' famous cowboy hat!
	* I also added a main landing page to the app for style and a little introduction as to the overall purpose of the app
	* I Added a Progress bar to be displayed whilst the data is being loaded from the network
	* I moved the Base API Endpoint out of the fragment(s) and into the build.config file for better security and easier management
	* I converted all models to Kotlin Data Classes, annotated the properties with GSON notation, and made the model classes inherit from serializable
	* I introduced and used a more robust logging framework - Timber
	* I switched the app’s theme from an appCompat theme to a MaterialComponents theme

For the ListView:
	* I passed the click listener into the adapter so the on click events could be handled within the fragment (versus in the adapter)
	* I added additional data fields as well as the author's image to be displayed in the list of blog posts, and significantly improved the layout
	* I consolidated the list view's view holder into the adapter class as an inner class

Future considerations / time pending:
	* I would have used Kotlin's data binding features
	* I would have introduced a more robust Dependency Injection (DI) framework such as Dagger 2
	* I would have added a more enterprise-grade local storage & caching mechanism like Google Room
	* I would have added Google analytics and Firebase crash reporting
	* I would have made use of a lot more Material Design elements
	* Finally, given more time, I would have written more, and more robust unit tests

Final Note:
I was able to get the local server to run, intermittently, on my local machine.
I was unhappy with this inconsistency due to my hardware, so I moved the data to different, more reliable hosted endpoint - http://media.kent.edu/~jhunter/bnr/
The endpoint can be easily updated in the app's build.config file, and different versions and flavors of the app can also have their own differing base url's.