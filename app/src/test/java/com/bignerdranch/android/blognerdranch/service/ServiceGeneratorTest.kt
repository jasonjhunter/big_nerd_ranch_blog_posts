package com.bignerdranch.android.blognerdranch.service

import com.bignerdranch.android.blognerdranch.BuildConfig
import org.junit.Test
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.nio.charset.Charset

class ServiceGeneratorTest {
    @Test
    @Throws(Exception::class)
    fun testAvailability() {
        val connection = URL(BuildConfig.API_URL).openConnection()
        val response = connection.getInputStream()
        val buffer = StringBuffer()
        BufferedReader(InputStreamReader(response, Charset.defaultCharset())).use { reader ->
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                buffer.append(line)
            }
        }
        assert(buffer.isNotEmpty())
    }
}