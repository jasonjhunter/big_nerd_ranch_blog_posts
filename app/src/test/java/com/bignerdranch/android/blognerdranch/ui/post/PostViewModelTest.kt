package com.bignerdranch.android.blognerdranch.ui.post

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.bignerdranch.android.blognerdranch.Event
import com.bignerdranch.android.blognerdranch.PostTypes
import com.bignerdranch.android.blognerdranch.model.Author
import com.bignerdranch.android.blognerdranch.model.Post
import com.bignerdranch.android.blognerdranch.model.PostMetadata
import com.bignerdranch.android.blognerdranch.model.Resource
import com.bignerdranch.android.blognerdranch.service.PostRepo
import com.bignerdranch.android.blognerdranch.util.observeOnce
import com.nhaarman.mockitokotlin2.*
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.util.*


@RunWith(JUnit4::class)
class PostViewModelTest {

    private lateinit var viewModel: PostViewModel
    private lateinit var postRepo: PostRepo

    private lateinit var postMetadataListObserver: Observer<Event<Resource<List<PostMetadata>>>>
    private lateinit var postObserver: Observer<Event<Resource<Post>>>

    private val POST_METADATA_ID_INVALID = 0
    private val POST_METADATA_ID = 1
    private val POST_METADATA_POST_ID = 1
    private val POST_METADATA_TITLE = "Post Metadata Title"
    private val POST_METADATA_SUMMARY = "Post Metadata Summary"

    private val POST_ID = 1
    private val POST_BODY = "Post Body"

    private val AUTHOR_NAME = "Author Name"
    private val AUTHOR_IMAGE_URL = "/images/authors/alex.jpg"
    private val AUTHOR_TITLE = "Author Title"
    private val AUTHOR_DATE = Date().toString()

    private val author = Author(AUTHOR_NAME, AUTHOR_IMAGE_URL, AUTHOR_TITLE)

    private val postMetadata0 = PostMetadata(
        POST_METADATA_ID_INVALID,
        POST_METADATA_POST_ID,
        POST_METADATA_TITLE,
        POST_METADATA_SUMMARY,
        author,
        AUTHOR_DATE
    )

    private var postMetadata = PostMetadata(
        POST_METADATA_ID, POST_METADATA_POST_ID, POST_METADATA_TITLE,
        POST_METADATA_SUMMARY, author, AUTHOR_DATE
    )

    private val postMetadataList = listOf(postMetadata0)

    private val post = Post(POST_ID, postMetadata, POST_BODY)

    private val postSuccessResource = Resource.success(post)
    private val postListSuccessResource = Resource.success(postMetadataList)
    private val errorResource = Resource.error("Some error happened", null)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {

        postRepo = mock()
        runBlocking {
            whenever(postRepo.getPost(POST_ID)).thenReturn(postSuccessResource)
            whenever(postRepo.getPost(POST_METADATA_ID_INVALID)).thenReturn(errorResource)

            whenever(postRepo.getPostList(PostTypes.BLOG.postTypeId)).thenReturn(
                postListSuccessResource
            )
            whenever(postRepo.getPostList(PostTypes.BLOG.postTypeId)).thenReturn(errorResource)
        }

        viewModel = PostViewModel(postRepo)

        postObserver = mock()
        postMetadataListObserver = mock()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
    }

    @Test
    fun testGetPostList() {

        viewModel.getPostList(1)

        viewModel.postListResponse.observeOnce {
            assertEquals(it.getContentIfNotHandled()?.data?.get(0)?.id, 1)
        }
    }

    @Test
    fun testGetPost() {
        runBlocking {
            viewModel.getPost(1)
        }
        viewModel.postResponse.observeOnce {
            assertEquals(it.getContentIfNotHandled()?.data?.id, 1)
        }
    }

    @Test
    fun getFromViewModelScope() {

        viewModel.getPost(1)

        runBlocking {
            delay(1_000)
            viewModel.postResponse.observeOnce {
                val postEvent: Event<Resource<Post>> = it
                System.out.println("postEvent " + postEvent)
                System.out.println("postEventPeekContent " + postEvent.peekContent())
                assertEquals(postEvent.getContentIfNotHandled()?.data?.id, 1)
            }
            delay(1_000)
            viewModel.postResponse.observeOnce {
                val postEvent: Event<Resource<Post>> = it
                System.out.println("2 postEvent " + postEvent)
                System.out.println("2 postEventPeekContent " + postEvent.peekContent())
                assertEquals(postEvent.getContentIfNotHandled()?.data?.id, 1)
            }
        }
        // Trigger an update, which starts a coroutine that updates the value
        runBlocking {
            viewModel.getPost(2)
        }

        // Get the new value
        runBlocking {
            viewModel.postResponse.observeOnce {
                System.out.println("IT: " + it)
                assertEquals(it.getContentIfNotHandled()?.data?.id, 2)
            }
        }
    }

    @Test
    fun testNull() {
        assertThat(viewModel.repo, notNullValue())

        assertThat(viewModel.postListResponse, notNullValue())
        verify(postRepo, never()).getPostList(1)

        assertThat(viewModel.postResponse, notNullValue())
        verify(postRepo, never()).getPost(1)
    }

    @Test
    fun doNotFetchWithoutObservers() {
        viewModel.getPostList(1)
        verify(postRepo, never()).getPostList(eq(1))

        viewModel.getPost(1)
        verify(postRepo, never()).getPost(eq(1))
    }
}