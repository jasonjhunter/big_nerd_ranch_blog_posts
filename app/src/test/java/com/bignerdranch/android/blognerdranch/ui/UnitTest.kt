package com.bignerdranch.android.blognerdranch.ui

import com.bignerdranch.android.blognerdranch.service.PostApi
import com.bignerdranch.android.blognerdranch.service.ServiceGenerator
import com.bignerdranch.android.blognerdranch.ui.main.MainActivity
import com.bignerdranch.android.blognerdranch.ui.post.PostDetailActivity
import com.bignerdranch.android.blognerdranch.ui.post.PostDetailFragment
import com.bignerdranch.android.blognerdranch.ui.post.PostListActivity
import com.bignerdranch.android.blognerdranch.ui.post.PostListFragment
import com.bignerdranch.android.blognerdranch.ui.splash.SplashActivity
import org.junit.Assert.assertNotNull
import org.junit.Test

class UnitTest {

    @Test
    fun activities_shouldNotBeNull() {
        val splashActivity = SplashActivity()
        assertNotNull(splashActivity)

        val mainActivity = MainActivity()
        assertNotNull(mainActivity)

        val postListActivity = PostListActivity()
        assertNotNull(postListActivity)

        val postDetailActivity = PostDetailActivity()
        assertNotNull(postDetailActivity)
    }

    @Test
    fun fragments_shouldNotBeNull() {
        val postListFragment = PostListFragment
        assertNotNull(postListFragment)

        val postDetailFragment = PostDetailFragment()
        assertNotNull(postDetailFragment)
    }

    @Test
    fun serviceGenerator_shouldNotBeNull() {
        val postApi: PostApi = ServiceGenerator.createService(PostApi::class.java)
        assertNotNull(postApi)
    }
}