package com.bignerdranch.android.blognerdranch.service

import com.bignerdranch.android.blognerdranch.model.Post
import com.bignerdranch.android.blognerdranch.model.Resource
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PostRepoTest {
    private lateinit var postRepo: PostRepo
    private val validLocation = 1
    private val invalidLocation = 0
    private val post1 = Post(id = 1, null, "body")
    private val postResourceSuccess: Resource<Post> = Resource.success(post1)
    private val postResourceError: Resource<Post> = Resource.error("error msg", null)

    @Before
    fun setUp() {
        postRepo = PostRepo()
    }

    @Test
    fun `test getPost when valid postId is requested, then post is returned`() =
        runBlocking {
            assertEquals(postResourceSuccess.data?.id, postRepo.getPost(validLocation).data?.id)
        }

    @Test
    fun `test getWeather when invalid location is requested, then error is returned`() =
        runBlocking {
            assertEquals(postResourceError.data, postRepo.getPost(invalidLocation).data)
        }

}