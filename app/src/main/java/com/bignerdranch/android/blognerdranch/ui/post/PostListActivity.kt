package com.bignerdranch.android.blognerdranch.ui.post

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bignerdranch.android.blognerdranch.R

class PostListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, PostListFragment.newInstance())
                .commitNow()
        }
    }
}