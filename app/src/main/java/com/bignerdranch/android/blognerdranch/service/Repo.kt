package com.bignerdranch.android.blognerdranch.service

import com.bignerdranch.android.blognerdranch.model.Post
import com.bignerdranch.android.blognerdranch.model.PostMetadata
import com.bignerdranch.android.blognerdranch.model.Resource
import com.bignerdranch.android.blognerdranch.model.Result

interface Repo {

    fun getPostList(postTypeId: Int): Result<List<PostMetadata>>

    fun getPost(id: Int): Resource<Post>
}