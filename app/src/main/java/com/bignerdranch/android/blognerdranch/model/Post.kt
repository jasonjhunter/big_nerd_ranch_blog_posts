package com.bignerdranch.android.blognerdranch.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Post(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("metadata")
    val metadata: PostMetadata? = null,
    @SerializedName("body")
    val body: String? = null
) : Parcelable