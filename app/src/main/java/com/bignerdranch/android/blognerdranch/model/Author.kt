package com.bignerdranch.android.blognerdranch.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Author(
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("image")
    val imageUrl: String? = null,
    @SerializedName("title")
    val title: String? = null
) : Serializable