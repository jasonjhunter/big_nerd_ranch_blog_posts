package com.bignerdranch.android.blognerdranch.service

import com.bignerdranch.android.blognerdranch.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.text.DateFormat
import java.util.concurrent.TimeUnit

// create a new REST client with the given API base url. Note: keyword 'object' is singleton in Kotlin
object ServiceGenerator {

    private var apiBaseUrl: String = BuildConfig.API_URL
    private var retrofitBuilder = initRetrofitBuilder()
    private var retrofit = retrofitBuilder.build()

    fun <T : Any> createService(apiInterface: Class<T>): T {
        return retrofit.create(apiInterface)
    }

    private fun initRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(apiBaseUrl)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(okHttpClient)
    }

    private val converterFactory: Converter.Factory
        get() {

            val gson: Gson = GsonBuilder()
                .serializeNulls()
                .setDateFormat(DateFormat.LONG)
                .setPrettyPrinting()
                .create()

            return GsonConverterFactory.create(gson)
        }

    private val okHttpClient: OkHttpClient
        get() {

            // 1.) create an OkHttpClient Builder (set appropriate properties)
            val okHttpClientBuilder = OkHttpClient.Builder()
                .connectTimeout(30 * 1000.toLong(), TimeUnit.SECONDS)
                .readTimeout(30 * 1000.toLong(), TimeUnit.SECONDS)
                .writeTimeout(30 * 1000.toLong(), TimeUnit.SECONDS)

            // 2.) add the interceptors to the builder
            val okHttpClientBuilderWithInterceptors =
                addInterceptors(
                    okHttpClientBuilder
                )

            // 3.) build the builder (aka: create the okHttpClient)
            return okHttpClientBuilderWithInterceptors.build()
        }

    private fun addInterceptors(okHttpClientBuilder: OkHttpClient.Builder): OkHttpClient.Builder {

        val httpLogging = HttpLoggingInterceptor()
        httpLogging.setLevel(
            level =
            if (BuildConfig.DEBUG)
                HttpLoggingInterceptor.Level.BODY
            else
                HttpLoggingInterceptor.Level.NONE   // do not log in Release builds
        )

        if (!okHttpClientBuilder.interceptors().contains(httpLogging))
            okHttpClientBuilder.addInterceptor(httpLogging)

        return okHttpClientBuilder
    }
}
