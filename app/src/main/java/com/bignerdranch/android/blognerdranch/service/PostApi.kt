package com.bignerdranch.android.blognerdranch.service

import com.bignerdranch.android.blognerdranch.model.Post
import com.bignerdranch.android.blognerdranch.model.PostMetadata
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Definition of REST endpoints
 */
interface PostApi {

    /**
     * List of Posts
     *
     * @return Posts repository
     */
    @GET("post-metadata")
    fun getPostMetadata(@Query("postTypeId") postTypeId: Int): Call<List<PostMetadata>>

    /**
     * Individual Post
     *
     * @param id the id of the post
     * @return Post
     */
    @GET("post/{id}")
    fun getPost(@Path("id") id: Int): Call<Post>

}