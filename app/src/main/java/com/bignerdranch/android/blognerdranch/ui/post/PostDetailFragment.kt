package com.bignerdranch.android.blognerdranch.ui.post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.bignerdranch.android.blognerdranch.BuildConfig
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.model.Post
import com.bignerdranch.android.blognerdranch.ui.BaseFragment
import com.squareup.picasso.Picasso

class PostDetailFragment : BaseFragment() {

    private lateinit var post: Post

    private var progressBar: ProgressBar? = null
    private var authorImage: ImageView? = null
    private var postTitle: TextView? = null
    private var postPublishDate: TextView? = null
    private var postAuthor: TextView? = null
    private var postBody: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            post = it.getParcelable<Post>(ARG_POST) as Post
            //activity?.setTitle(post?.title)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(R.layout.fragment_post_detail, null) as ViewGroup
        progressBar = rootView.findViewById(R.id.progressbar_loading)
        authorImage = rootView.findViewById(R.id.author_imageView)
        postTitle = rootView.findViewById(R.id.title_textView)
        postPublishDate = rootView.findViewById(R.id.publishDate_textView)
        postAuthor = rootView.findViewById(R.id.author_textView)
        postBody = rootView.findViewById(R.id.body_textView)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        displayPost()
    }

    private fun displayPost() {

        hideLoading(progressBar!!)

        if (post != null) {
            postTitle!!.text = post.metadata!!.title
            postPublishDate!!.text = post.metadata!!.publishDate
            postAuthor!!.text = post.metadata!!.author?.name
            postBody!!.text = post.body
            //authorImage!!.setImageDrawable(getResources().getDrawable(R.drawable.bnr_hat, requireActivity().getApplicationContext().getTheme()));
            val imageUri = BuildConfig.API_URL + post.metadata!!.author!!.imageUrl
            Picasso.get().load(imageUri).into(authorImage)
        } else {
            // display an error
            Toast.makeText(activity, getString(R.string.big_nerd_ranch_error), Toast.LENGTH_SHORT)
                .show()
        }
    }

    companion object {
        private val TAG = PostDetailFragment::class.simpleName

        const val ARG_POST = "ARG_POST_METADATA"

        @JvmStatic
        fun newInstance(post: Post) =
            PostDetailFragment()
                .apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_POST, post)
                    }
                }
    }
}