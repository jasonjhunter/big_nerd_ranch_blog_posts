package com.bignerdranch.android.blognerdranch.ui.post

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bignerdranch.android.blognerdranch.Event
import com.bignerdranch.android.blognerdranch.PostTypes
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.Status
import com.bignerdranch.android.blognerdranch.model.Post
import com.bignerdranch.android.blognerdranch.model.PostMetadata
import com.bignerdranch.android.blognerdranch.model.Resource
import com.bignerdranch.android.blognerdranch.model.Result
import com.bignerdranch.android.blognerdranch.service.PostRepo
import com.bignerdranch.android.blognerdranch.ui.BaseFragment
import timber.log.Timber

class PostListFragment : BaseFragment(), BaseFragment.ListItemClickedListener<PostMetadata> {
    // Whether or not the activity is in two-pane mode, i.e. running on a tablet device
    private var twoPane: Boolean = false
    private var postRecyclerView: RecyclerView? = null
    private var itemDetailContainer: View? = null
    private var progressBar: ProgressBar? = null

    //  ******************** ViewModel initialization ********************

    // Get a reference to the ViewModel scoped to this Fragment
    private val viewModel by viewModels<PostViewModel> {
        PostViewModelFactory(PostRepo())
    }

    //  ******************** Observer initialization ********************
    private val postListObserver: Observer<Event<Result<List<PostMetadata>>>> =
        Observer { postList ->
            postList.getContentIfNotHandled()
                ?.let { // Only proceed if the event has never been handled
                    when (it) {
                        is Result.Error.RecoverableError,
                        is Result.Error.NonRecoverableError -> showError(
                            getString(
                                R.string.big_nerd_ranch_error,
                                it,
                            ),
                            progressBar!!
                        )
                        is Result.InProgress -> {
                            showLoading(
                                getString(R.string.big_nerd_ranch_loading),
                                progressBar!!
                            )
                        }
                        is Result.Success -> {
                            hideLoading(progressBar!!)
                            setupRecyclerView(it.data)
                        }
                    }
                }
        }

    //  ******************** Observer initialization ********************
    private val postObserver: Observer<Event<Resource<Post>>> =
        Observer { post ->
            post.getContentIfNotHandled()?.let { // Only proceed if the event has never been handled
                when (it.status) {
                    Status.SUCCESS -> {
                        displayPost(it.data!!)
                    }
                    Status.ERROR -> showError(
                        getString(
                            R.string.big_nerd_ranch_error,
                            it.message
                        ),
                        progressBar!!
                    )
                    Status.LOADING -> showLoading(
                        getString(R.string.big_nerd_ranch_loading),
                        progressBar!!
                    )
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(R.layout.fragment_post_list, null) as ViewGroup

        itemDetailContainer = rootView.findViewById(R.id.item_detail_container)
        postRecyclerView = rootView.findViewById(R.id.post_list_recyclerview)
        progressBar = rootView.findViewById(R.id.progressbar_loading)

        postRecyclerView?.layoutManager = LinearLayoutManager(requireActivity())
        postRecyclerView?.setHasFixedSize(true)

        if (itemDetailContainer != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        viewModel =
//            ViewModelProvider(
//                this,
//                PostViewModelFactory(
//                    PostRepo()
//                )
//            ).get(PostViewModel::class.java)

        viewModel.getPostList(PostTypes.BLOG.postTypeId)

        // Observe post data
        viewModel.postListResponse.observe(requireActivity(), postListObserver)
        viewModel.postResponse.observe(requireActivity(), postObserver)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_post_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_refresh -> {
                viewModel.clearPosts()
                viewModel.getPostList(PostTypes.BLOG.postTypeId)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        super.onPause()
        if (twoPane) {
            val fragment: Fragment? =
                requireActivity().supportFragmentManager.findFragmentByTag(POST_DETAIL_FRAGMENT_TAG)
            if (fragment != null) requireActivity().supportFragmentManager.beginTransaction()
                .remove(fragment)
                .commit()
        }
    }

    private fun setupRecyclerView(list: List<PostMetadata>) {
        Timber.d("Timber Debug: setupRecyclerView  - %s", list.size)

        hideLoading(progressBar!!)

        val postMetadataAdapter =
            PostAdapter(list) { postMetadata: PostMetadata ->
                listItemClicked(
                    postMetadata
                )
            }

        postRecyclerView?.adapter = postMetadataAdapter
    }

    private fun displayPost(post: Post) {
        Timber.d("Timber Debug: displayPost  - %s", post.metadata!!.title)

        hideLoading(progressBar!!)

        if (twoPane) {
            val postDetailFragment =
                PostDetailFragment.newInstance(post)

            replaceFragment(
                R.id.item_detail_container,
                postDetailFragment,
                POST_DETAIL_FRAGMENT_TAG
            )

        } else {
            val intentPostDetail = Intent(requireContext(), PostDetailActivity::class.java).apply {
                putExtra(PostDetailFragment.ARG_POST, post)
            }

            startActivity(intentPostDetail)
        }
    }

    override fun listItemClicked(postMetadata: PostMetadata) {
        Timber.d("Timber Debug: listItemClicked  - %s", postMetadata)

        if (postMetadata.postId != null) {
            Timber.d("Timber Debug: %s", "Displaying post")
            viewModel.getPost(postMetadata.postId)             // trigger value change
        }
    }

    companion object {
        private val TAG = PostListFragment::class.java.simpleName
        private const val POST_DETAIL_FRAGMENT_TAG = "POST_DETAIL_FRAGMENT_TAG"

        @JvmStatic
        fun newInstance() = PostListFragment()
    }
}