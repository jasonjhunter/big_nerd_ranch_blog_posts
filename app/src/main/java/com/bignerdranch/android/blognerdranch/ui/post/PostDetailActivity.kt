package com.bignerdranch.android.blognerdranch.ui.post

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.model.Post

class PostDetailActivity : AppCompatActivity() {

    private var post: Post? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_detail)


        post = intent.extras?.getParcelable(PostDetailFragment.ARG_POST) as Post?

        if (post != null) {
            this.title = post?.metadata?.title

            if (savedInstanceState == null) {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container, PostDetailFragment.newInstance(post!!))
                    .commitNow()
            }
        }
    }

    companion object {
        private val TAG = PostDetailActivity::class.simpleName
    }
}
