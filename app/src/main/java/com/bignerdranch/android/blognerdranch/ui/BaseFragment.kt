package com.bignerdranch.android.blognerdranch.ui

import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    // Replace current Fragment with the destination Fragment
    fun replaceFragment(containerViewId: Int, fragment: Fragment, tag: String? = null) {
        childFragmentManager.beginTransaction()
                .replace(containerViewId, fragment, tag).commitNow()
    }

    fun showLoading(
        message: String,
        progressBar: ProgressBar
    ) {
        progressBar.visibility = View.VISIBLE
    }

    fun hideLoading(progressBar: ProgressBar) {
        progressBar.visibility = View.GONE
    }

    fun showError(
        message: String,
        progressBar: ProgressBar
    ) {

        hideLoading(progressBar)
    }

    interface ListItemClickedListener<in T> {
        fun listItemClicked(item: T)
    }

    companion object {
        private val TAG = BaseFragment::class.java.simpleName
    }
}