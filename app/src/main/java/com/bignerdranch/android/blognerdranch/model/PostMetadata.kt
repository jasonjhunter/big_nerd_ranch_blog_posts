package com.bignerdranch.android.blognerdranch.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class PostMetadata(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("postId")
    val postId: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("summary")
    val summary: String? = null,
    @SerializedName("author")
    val author: Author? = null,
    @SerializedName("publishDate")
    val publishDate: String? = null
) : Parcelable
