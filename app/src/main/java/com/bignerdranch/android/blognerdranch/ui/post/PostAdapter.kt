package com.bignerdranch.android.blognerdranch.ui.post

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bignerdranch.android.blognerdranch.BuildConfig
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.model.PostMetadata
import com.squareup.picasso.Picasso

class PostAdapter(
    private var postMetadata: List<PostMetadata>,
    private val clickListener: (PostMetadata) -> Unit
) : RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    override fun getItemCount(): Int {
        return postMetadata.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_post_list_linear, parent, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val post = postMetadata[position]
        holder.bind(post, clickListener)
    }

    inner class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var postMetadata: PostMetadata? = null

        //private val postIdTextView: TextView = itemView.findViewById(R.id.postId_textView)
        private val postImageView: ImageView = itemView.findViewById(R.id.post_imageView)
        private val postTitleTextView: TextView = itemView.findViewById(R.id.title_textView)
        //private val postAuthorTextView: TextView = itemView.findViewById(R.id.author_textView)
        private val postSummaryTextView: TextView = itemView.findViewById(R.id.summary_textView)
        private val postPublishDateTextView: TextView = itemView.findViewById(R.id.publishDate_textView)

        fun bind(postMetadata: PostMetadata, clickListener: (PostMetadata) -> Unit) {
            this.postMetadata = postMetadata
            //postIdTextView.text = postMetadata.postId.toString()
            val imageUri = BuildConfig.API_URL + postMetadata.author!!.imageUrl
            Picasso.get().load(imageUri).into(postImageView)
            postTitleTextView.text = postMetadata.title
            //postAuthorTextView.text = postMetadata.author?.name
            postSummaryTextView.text = postMetadata.summary
            postPublishDateTextView.text = postMetadata.publishDate

            itemView.setOnClickListener { clickListener(postMetadata) } // populate click listener
        }
    }

}