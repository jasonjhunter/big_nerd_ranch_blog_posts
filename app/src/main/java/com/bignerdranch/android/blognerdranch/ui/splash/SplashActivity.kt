package com.bignerdranch.android.blognerdranch.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.ui.main.MainActivity


class SplashActivity : AppCompatActivity() {

    val delayMillis = 6000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        makeFullScreen()

        setContentView(R.layout.activity_splash)

        val imageView = findViewById<ImageView>(R.id.img_logo)

        rotateImage(imageView)

        // Using a handler to delay loading MainActivity
        Handler().postDelayed({
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )  // Animate loading
            finish()
        }, delayMillis)
    }

    private fun makeFullScreen() {

        requestWindowFeature(Window.FEATURE_NO_TITLE)   // Remove Title

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        supportActionBar?.hide()    // Hide the toolbar
    }

    private fun rotateImage(imageView: ImageView) {

        val imageViewRotationRunnable: Runnable = object : Runnable {
            override fun run() {
                imageView.animate().rotationBy(360f).withEndAction(this).setDuration(delayMillis)
                    .setInterpolator(LinearInterpolator()).start()
            }
        }

        imageView.animate().rotationBy(360f).withEndAction(imageViewRotationRunnable)
            .setDuration(delayMillis)
            .setInterpolator(LinearInterpolator()).start()
    }
}
