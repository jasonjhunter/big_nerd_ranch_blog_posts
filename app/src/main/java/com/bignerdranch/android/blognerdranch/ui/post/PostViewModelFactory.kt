package com.bignerdranch.android.blognerdranch.ui.post

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bignerdranch.android.blognerdranch.service.Repo

/**
 * Factory for creating a [PostViewModel] with a constructor that takes a [Repo].
 * @param repo
 */
/*
class PostViewModelFactoryX(private val repo: Repo) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = PostViewModel(
        repo
    ) as T
}
*/

class PostViewModelFactory(private val repo: Repo) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        modelClass.getConstructor(Repo::class.java)
            .newInstance(repo)
}