package com.bignerdranch.android.blognerdranch.ui.post

import androidx.lifecycle.*
import com.bignerdranch.android.blognerdranch.Event
import com.bignerdranch.android.blognerdranch.model.Post
import com.bignerdranch.android.blognerdranch.model.PostMetadata
import com.bignerdranch.android.blognerdranch.model.Resource
import com.bignerdranch.android.blognerdranch.model.Result
import com.bignerdranch.android.blognerdranch.service.Repo
import kotlinx.coroutines.Dispatchers
import timber.log.Timber
import java.io.IOException

class PostViewModel(val repo: Repo) : ViewModel() {

    private var cachedPosts: MutableList<Post> = mutableListOf()
    private var cachedPostList: List<PostMetadata> = emptyList()

    fun clearPosts() {
        cachedPosts.clear()
        cachedPostList = emptyList()
    }

    ////////

    private val _getPostList = MutableLiveData<Int>()

    fun getPostList(postTypeId: Int) {
        _getPostList.value = postTypeId     // Trigger the value change
    }

    val postListResponse: LiveData<Event<Result<List<PostMetadata>>>> =
        _getPostList.switchMap { postTypeId ->
            liveData(Dispatchers.IO) {
                try {
                    emit(Event(Result.InProgress))
                    if (cachedPostList.isEmpty()) {
                        repo.getPostList(postTypeId).let {
                            when (it) {
                                is Result.Success -> {
                                    cachedPostList = it.data
                                    emit(Event(it))
                                }
                                else -> {
                                    emit(Event(Result.Error.NonRecoverableError(Exception("Something wacky occurred"))))
                                }
                            }
                        }
                    } else {
                        emit(Event(Result.Success(cachedPostList)))
                    }
                } catch (e: IOException) {
                    Timber.e(e)
                    emit(Event(Result.Error.RecoverableError(e)))
                }
            }
        }

    ////////

    private val _getPost = MutableLiveData<Int>()

    fun getPost(input: Int) {
        _getPost.value = input  // Trigger the value change
    }

    var postResponse: LiveData<Event<Resource<Post>>> =
        _getPost.switchMap { postId ->
            liveData(Dispatchers.IO) {
                emit(Event(Resource.loading(null)))
                if (postId != null) {
                    try {
                        var post = cachedPosts.find { it.id == postId }
                        if (post == null) {
                            post = repo.getPost(postId).data
                        }
                        cachedPosts.add(post!!)
                        emit(Event(Resource.success(post)))
                    } catch (e: Exception) {
                        Timber.e(e)
                        emit(Event(Resource.error(e.toString(), null)))
                    }
                }
            }
        }
}