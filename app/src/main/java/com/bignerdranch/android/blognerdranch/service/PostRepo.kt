package com.bignerdranch.android.blognerdranch.service

import com.bignerdranch.android.blognerdranch.model.*
import com.bignerdranch.android.blognerdranch.model.Result.Error
import timber.log.Timber
import java.io.IOException

/**
 * Initiates a network request and returns a Resource object via Post and List<Post> objects
 */
class PostRepo : Repo {

    /**
     * this method returns all posts
     *
     * @return Resource<List<PostMetaData>
     */
    override fun getPostList(postTypeId: Int): Result<List<PostMetadata>> {
        Timber.d("Timber Debug: %s", "getPostList")

        try {
            val response = postApi.getPostMetadata(postTypeId).execute()
            return when (val apiResponse = ApiResponse.create(response)) {
                is ApiSuccessResponse -> {
                    Result.Success(apiResponse.body)
                }
                is ApiEmptyResponse -> {
                    Error.RecoverableError(Exception("ApiEmptyResponse"))
                }
                is ApiErrorResponse -> {
                    Error.NonRecoverableError(Exception(apiResponse.errorMessage))
                }
            }
        } catch (e: IOException) {
            Timber.e(e)
            return Error.NonRecoverableError(e)
        }
    }

    /**
     * this method returns a single post
     *
     * @param id the id of the requested post
     * @return Resource<Post>
     */
    override fun getPost(id: Int): Resource<Post> {
        Timber.d("getPost")

        try {
            val response = postApi.getPost(id).execute()
            return when (val apiResponse = ApiResponse.create(response)) {
                is ApiSuccessResponse -> {
                    Resource.success(apiResponse.body)
                }
                is ApiEmptyResponse -> {
                    Resource.success(null)
                }
                is ApiErrorResponse -> {
                    Resource.error(
                        apiResponse.errorMessage,
                        null
                    )
                }
            }
        } catch (e: IOException) {
            Timber.e(e)
            return Resource.error(e.toString(), null)
        }
    }

    companion object {
        private val TAG = PostApi::class.java.simpleName

        val postApi: PostApi
            get() {
                return ServiceGenerator.createService(PostApi::class.java)
            }
    }
}
