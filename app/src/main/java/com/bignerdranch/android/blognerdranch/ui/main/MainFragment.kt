package com.bignerdranch.android.blognerdranch.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.ui.BaseFragment
import com.bignerdranch.android.blognerdranch.ui.post.PostListActivity

class MainFragment : BaseFragment() {

    private var btnViewPosts: Button? = null

    companion object {
        fun newInstance() =
            MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(R.layout.fragment_main, null) as ViewGroup

        btnViewPosts = rootView.findViewById(R.id.btnViewPosts)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnViewPosts?.setOnClickListener {
            val intent = Intent(requireContext(), PostListActivity::class.java)
            startActivity(intent)
        }
    }

}