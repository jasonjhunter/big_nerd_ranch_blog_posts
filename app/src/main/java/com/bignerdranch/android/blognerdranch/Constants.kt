package com.bignerdranch.android.blognerdranch

/**
 * Status of a resource that is provided to the UI.
 *
 * These are usually created by the Repository classes where they return
 * `LiveData<Resource<T>>` to pass back the latest data to the UI with its fetch status.
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}

/**
 * Post types and assumed ID's
 */
enum class PostTypes(val postTypeId: Int) {
    BLOG(1),
    SOCIAL(2),
    OTHER(3);
}